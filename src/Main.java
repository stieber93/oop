public class Main {

    public static void main(String[] args) {

        Mitarbeiter mitarbeiter1 = new Mitarbeiter();
        Mitarbeiter mitarbeiter2 = new Mitarbeiter();
        Mitarbeiter mitarbeiter3 = new Mitarbeiter();
        Mitarbeiter mitarbeiter4 = new Mitarbeiter();

        mitarbeiter1.setId(1);
        mitarbeiter1.setName("Albert");

        mitarbeiter2.setId(2);
        mitarbeiter2.setName("Bernd");

        mitarbeiter3.setId(3);
        mitarbeiter3.setName("Chris");

        mitarbeiter4.setId(4);
        mitarbeiter4.setName("Dave");


        System.out.println(mitarbeiter1);
        System.out.println(mitarbeiter2);
        System.out.println(mitarbeiter3);
        System.out.println(mitarbeiter4);


    }
}
